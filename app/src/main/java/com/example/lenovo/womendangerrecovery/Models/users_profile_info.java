package com.example.lenovo.womendangerrecovery.Models;

public class users_profile_info
{

    private String Hemail1;
    private String hmobile1;
    private String Hemail2;
    private String hmobile2;
    private String Hemail3;
    private String hmobile3;

    public users_profile_info(String Hemail1,String hmobile1,String Hemail2,String hmobile2,String Hemail3,String hmobile3)
    {

        this.Hemail1=Hemail1;
        this.hmobile1=hmobile1;
        this.Hemail2=Hemail2;
        this.hmobile2=hmobile2;
        this.Hemail3=Hemail3;
        this.hmobile3=hmobile3;
    }

    public users_profile_info()
    {

    }


    public String getHemail1() {
        return Hemail1;
    }

    public void setHemail1(String Hemail1) {
        this.Hemail1 = Hemail1;
    }

    public String getHmobile1() {
        return hmobile1;
    }

    public void setHmobile1(String hmobile1) {
        this.hmobile1 = hmobile1;
    }

    public String getHemail2() {
        return Hemail2;
    }

    public void setHemail2(String Hemail2) {
        this.Hemail2 = Hemail2;
    }

    public String getHmobile2() {
        return hmobile2;
    }

    public void setHmobile2(String hmobile2) {
        this.hmobile2 = hmobile2;
    }

    public String getHemail3() {
        return Hemail3;
    }

    public void setHemail3(String Hemail3) {
        this.Hemail3 = Hemail3;
    }

    public String getHmobile3() {
        return hmobile3;
    }

    public void setHmobile3(String hmobile3) {
        this.hmobile3 = hmobile3;
    }


    @Override
    public String toString() {
        return "users_profile_info{" +

                ", Hemail1='" + Hemail1 + '\'' +
                ", hmobile1='" + hmobile1 + '\'' +
                ", Hemail2='" + Hemail2 + '\'' +
                ", hmobile2='" + hmobile2 + '\'' +
                ", Hemail3='" + Hemail3 + '\'' +
                ", hmobile3='" + hmobile3 + '\'' +
                '}';
    }
}

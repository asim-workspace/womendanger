package com.example.lenovo.womendangerrecovery;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.womendangerrecovery.Models.user_account_settings;
import com.example.lenovo.womendangerrecovery.Models.users_profile_info;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SendMessActivity extends AppCompatActivity {
    private static final String TAG="SendMessActivity";
    private String Hmobile1,Hmobile2,Hmobile3,m1,m2,m3,eMessage;
    private TextView e1,e2,e3,e4;
    private String mess="I am in Danger Please save me";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendmess);

        e1=findViewById(R.id.mobile1);
        e2=findViewById(R.id.mobile2);
        e3=findViewById(R.id.mobile3);
        e4=findViewById(R.id.message);


        Button send = (Button) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                sendMess();
                }
        });

setupFirebaseAuth();
    }

    public void sendMess()
    {
        m1=e1.getText().toString();
        m2=e2.getText().toString();
        m3=e3.getText().toString();
        eMessage=e4.getText().toString();

        String numbers[] = {m1,m2,m3};

        for(String number : numbers)
        {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, eMessage, null, null);

            Toast.makeText(SendMessActivity.this, "Sent !", Toast.LENGTH_SHORT).show();

        }
    }


    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: Setting up Firebase Auth");

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();


                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged: signed in:" + user.getUid());
                } else {
                    Log.d(TAG, "onAuthStateChanged: sign out:");
                }

            }
        };
//          myRef= FirebaseDatabase.getInstance().getReference("user_account_settings");

        myRef= FirebaseDatabase.getInstance().getReference();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    users_profile_info upi = ds.child(mAuth.getCurrentUser().getUid()).getValue(users_profile_info.class);

//                    Hemail1 = upi.getHemail1();
                    Hmobile1 = upi.getHmobile1();
//                    Hemail2 = upi.getHemail2();
                    Hmobile2 = upi.getHmobile2();
//                    Hemail3 = upi.getHemail3();
                    Hmobile3 = upi.getHmobile3();

//                    dialog.dismiss();
//                    if(Hemail1.isEmpty()&&Hmobile1.isEmpty()&&Hemail2.isEmpty()&&Hmobile2.isEmpty()&&Hemail3.isEmpty()&&Hmobile3.isEmpty())
//                    {
//                        Toast.makeText(mContext, "please add emergency contacts", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//                    e1.setText(Hemail1);
                    e1.setText(Hmobile1);
//                    e3.setText(Hemail2);
                    e2.setText(Hmobile2);
//                    e5.setText(Hemail3);
                    e3.setText(Hmobile3);
                    e4.setText(mess);
//                    e4.setText(address);
//                    circleImageView.setImageURI(Uri.parse(photo));
//                    String ssname=username;
//                   Intent intent=new Intent(getActivity(),Account_Activity.class);
//                   intent.putExtra("uname",ssname);
//                   startActivity(intent);
//                   }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

    }

    public void onStop() {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);

    }

}

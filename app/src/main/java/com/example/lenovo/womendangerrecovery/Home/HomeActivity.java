package com.example.lenovo.womendangerrecovery.Home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidhiddencamera.CameraConfig;
import com.androidhiddencamera.CameraError;
import com.androidhiddencamera.HiddenCameraActivity;
import com.androidhiddencamera.HiddenCameraUtils;
import com.androidhiddencamera.config.CameraFacing;
import com.androidhiddencamera.config.CameraImageFormat;
import com.androidhiddencamera.config.CameraResolution;
import com.androidhiddencamera.config.CameraRotation;
import com.example.lenovo.womendangerrecovery.Login.LoginActivity;
import com.example.lenovo.womendangerrecovery.Main3Activity;
import com.example.lenovo.womendangerrecovery.Models.users_profile_info;
import com.example.lenovo.womendangerrecovery.R;
import com.example.lenovo.womendangerrecovery.Utils.BottomNavigationViewHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.io.File;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends HiddenCameraActivity implements OnMapReadyCallback,LocationListener {
    private static final String TAG="HomeActivity";
    private static final int ACTIVITY_NUM=0;
    private Context mContext=HomeActivity.this;
    public static String s,d,t;
    private ProgressDialog progressDialog;
    private GoogleMap mMap;
    LocationManager locationManager;
//    public static String s;

    private static final int REQ_CODE_CAMERA_PERMISSION = 1253;

    private CameraConfig mCameraConfig,mCameraConfig2;
    ImageView in;

    private String Hmobile1,Hmobile2,Hmobile3,m1,m2,m3,eMessage,locations,eMessage2,em1,em2,em3;
    private TextView e1,e2,e3,e4,e5;
    private String mess="I am in Danger Please save me";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private Button b1,b2,b3,b4,b5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });

//        progressDialog=new ProgressDialog(this);
//        progressDialog.show();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this,
//                Manifest.permission.SEND_SMS))
//        {
//
//        }else
//        {
        Toast.makeText(mContext, "Please grant all the  permissions from your phone settings", Toast.LENGTH_SHORT).show();
//        }

        e1=findViewById(R.id.mobile1);
        e2=findViewById(R.id.mobile2);
        e3=findViewById(R.id.mobile3);
        e4=findViewById(R.id.message);
        e5=findViewById(R.id.location);
        b3=findViewById(R.id.helpp2);

        b5=findViewById(R.id.image1);
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        b4=findViewById(R.id.helpp3);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (e5.getText().toString().equals(""))
                {

                    Toast.makeText(mContext, "Please swipe down for ur location...!!", Toast.LENGTH_SHORT).show();
                }
//                progressDialog.setMessage("please wait...");
//                progressDialog.show();
//                sendmess2();
//                siron();

                else{
//                    takePicture();
//                    sendImage();

                    sendmail1();
                    sendmail2();
                    sendmail3();
                    siron();

                }}
        });
        progressDialog=new ProgressDialog(this);
        locations=s;
//      String location=getIntent().getStringExtra("loadsPosition");
//         locations=location;
//        Bundle bundle=getIntent().getExtras();
//      String location =bundle.getString("loc");


        b2=findViewById(R.id.signout);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("please wait...");
                progressDialog.show();
                mAuth.signOut();
                finish();
                progressDialog.dismiss();
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (e5.getText().toString().equals(""))
                {

                    Toast.makeText(mContext, "Please wait for ur location...!!", Toast.LENGTH_SHORT).show();
                }else {
//                    progressDialog.setMessage("please wait...");
//                    progressDialog.show();
                    sendMess();
                    siron();
                }
            }
        });
        b1=findViewById(R.id.helpp);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (e5.getText().toString().equals(""))
                {
                    Toast.makeText(mContext, "Please wait for ur location...!!", Toast.LENGTH_SHORT).show();
                }else
                {



//                    progressDialog.setMessage("please wait...");
//                    progressDialog.show();
                    sendMess();
                    sendmail1();
                    sendmail2();
                    sendmail3();
                    sendImage1();
                    sendImage2();
                    sendImage3();
//                    sendImage();
                    siron();
//                    take();
















                }

//                siron();
            }
        });
//        click();

        //-------------------camera-----------------\

        mCameraConfig = new CameraConfig()
                .getBuilder(this)
                .setCameraFacing(CameraFacing.REAR_FACING_CAMERA)
                .setCameraResolution(CameraResolution.HIGH_RESOLUTION)
                .setImageFormat(CameraImageFormat.FORMAT_JPEG)

//                .setImageRotation(CameraRotation.ROTATION_270)
                .build();


        //Check for the camera permission for the runtime
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {

            //Start camera preview
            startCamera(mCameraConfig);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQ_CODE_CAMERA_PERMISSION);
        }

        take();
        in=findViewById(R.id.img1);
        in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });




        setupBottomNavigationView();
        setupFirebaseAuth();
        takePicture();
    }

    public void refreshData()
    {

        recreate();

    }


//public void click()
//{
//    if (e5.toString().equals(""))
//    {
//        b4.setEnabled(false);
//    }
//}



    public void sendMess()
    {
        progressDialog.setMessage("please wait...");
        progressDialog.show();


        m1=e1.getText().toString();
        m2=e2.getText().toString();
        m3=e3.getText().toString();
        eMessage=e4.getText().toString();
        eMessage2=e5.getText().toString();

//        locations=e5.getText().toString();


        String numbers[] = {m1,m2,m3};
        String messages[]={eMessage,eMessage2};
        for(String number : numbers)
        {

            for(String message : messages)
            {
                SmsManager smsManager1 = SmsManager.getDefault();
                smsManager1.sendTextMessage(number, null, message, null, null);
                progressDialog.dismiss();
                Toast.makeText(mContext, "Message Sent!!", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void siron()
    {
        final MediaPlayer catSoundMediaPlayer = MediaPlayer.create(this, R.raw.sirens);
        catSoundMediaPlayer.start();
    }

    public void setupBottomNavigationView()
    {
        Log.d(TAG, "setupBottomNavigationView: Settinh up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx= findViewById(R.id.navigation);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext,bottomNavigationViewEx);
        Menu menu=bottomNavigationViewEx.getMenu();
        MenuItem menuItem=menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }

    public void sendImage1()
    {
        progressDialog.setMessage("please wait...");
        progressDialog.show();
        BackgroundMail bm = new BackgroundMail(mContext);
        bm.setGmailUserName("womenwomendanger225@gmail.com");
        //"DoE/GTiYpX5sz5zmTFuoHg==" is crypted "password"
        bm.setGmailPassword("puneetismummy");
        bm.setMailTo(em1);
        bm.setFormSubject("Women Danger ");
        bm.setFormBody(mess +"  "+ locations);
        bm.setAttachment(t);
        bm.send();
    }

    public void sendImage2()
    {
        BackgroundMail bm = new BackgroundMail(mContext);
        bm.setGmailUserName("womenwomendanger225@gmail.com");
        //"DoE/GTiYpX5sz5zmTFuoHg==" is crypted "password"
        bm.setGmailPassword("puneetismummy");
        bm.setMailTo(em2);
        bm.setFormSubject("Women Danger ");
        bm.setFormBody(mess +"  "+ locations);
        bm.setAttachment(t);
        bm.send();
    }

    public void sendImage3()
    {
        BackgroundMail bm = new BackgroundMail(mContext);
        bm.setGmailUserName("womenwomendanger225@gmail.com");
        //"DoE/GTiYpX5sz5zmTFuoHg==" is crypted "password"
        bm.setGmailPassword("puneetismummy");
        bm.setMailTo(em3);
        bm.setFormSubject("Women Danger ");
        bm.setFormBody(mess +"  "+ locations);
        bm.setAttachment(t);
        bm.send();
        progressDialog.dismiss();
    }

    public void sendmail1()
    {
        progressDialog.setMessage("please wait...");
        progressDialog.show();
        BackgroundMail bm = new BackgroundMail(mContext);
        bm.setGmailUserName("womenwomendanger225@gmail.com");
        //"DoE/GTiYpX5sz5zmTFuoHg==" is crypted "password"
        bm.setGmailPassword("puneetismummy");
        bm.setMailTo(em1);
        bm.setFormSubject("Women Danger ");
        bm.setFormBody(mess +"  "+ locations);
        bm.send();
    }

    public void sendmail2()
    {
//        progressDialog.setMessage("please wait...");
//        progressDialog.show();
        BackgroundMail bm = new BackgroundMail(mContext);
        bm.setGmailUserName("womenwomendanger225@gmail.com");
        //"DoE/GTiYpX5sz5zmTFuoHg==" is crypted "password"
        bm.setGmailPassword("puneetismummy");
        bm.setMailTo(em2);
        bm.setFormSubject("Women Danger ");
        bm.setFormBody(mess +"  "+ locations);
        bm.send();
    }

    public void sendmail3()
    {
//        progressDialog.setMessage("please wait...");
//        progressDialog.show();
        BackgroundMail bm = new BackgroundMail(mContext);
        bm.setGmailUserName("womenwomendanger225@gmail.com");
        //"DoE/GTiYpX5sz5zmTFuoHg==" is crypted "password"
        bm.setGmailPassword("puneetismummy");
        bm.setMailTo(em3);
        bm.setFormSubject("Women Danger ");
        bm.setFormBody(mess +"  "+ locations);
        bm.send();
        progressDialog.dismiss();
    }

    public void sendmess2()
    {
        progressDialog.setMessage("please wait...");
        progressDialog.show();
        m1=e1.getText().toString();
        m2=e2.getText().toString();
        m3=e3.getText().toString();
        eMessage=e4.getText().toString();

        String numbers[] = {m1,m2,m3};
//        String messages[]={eMessage,eMessage2};
        for(String number : numbers)
        {

//            for(String message : messages)

            SmsManager smsManager1 = SmsManager.getDefault();
            smsManager1.sendTextMessage(number, null, eMessage, null, null);
            progressDialog.dismiss();
            Toast.makeText(mContext, "Message Sent!!", Toast.LENGTH_SHORT).show();

        }


    }







    /*------------------------------------FIREBASE DATABASE-------------------------------*/


    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: Setting up Firebase Auth");

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();


                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged: signed in:" + user.getUid());
                } else {
                    Log.d(TAG, "onAuthStateChanged: sign out:");
                    Intent intent=new Intent(mContext, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

            }
        };

        /*------------------------------------SEND MESSAGE-------------------*/

        myRef= FirebaseDatabase.getInstance().getReference();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                    d= String.valueOf(myRef.child("users_profile_info"));
//                    if(d.isEmpty()) {
                    String Uid =mAuth.getCurrentUser().getUid();
                    users_profile_info upi = ds.child(mAuth.getCurrentUser().getUid()).getValue(users_profile_info.class);
//                    String d= String.valueOf(myRef.child(mAuth.getCurrentUser().getUid()).child("hemail1"));
//                    if (myRef.getDatabase().equals(null))
//                    {
                    em1 = upi.getHemail1();
                    Hmobile1 = upi.getHmobile1();
                    em2 = upi.getHemail2();
                    Hmobile2 = upi.getHmobile2();
                    em3 = upi.getHemail3();
                    Hmobile3 = upi.getHmobile3();

//                    e1.setText(Hemail1);
                    e1.setText(Hmobile1);
//                    e3.setText(Hemail2);
                    e2.setText(Hmobile2);
//                    e5.setText(Hemail3);
                    e3.setText(Hmobile3);
                    e4.setText(mess);
                    e5.setText(locations);

//                    }else
//                    {
//                        Toast.makeText(mContext, "update data first!!", Toast.LENGTH_SHORT).show();
//                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

    }

    public void onStop() {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);

    }


    /*---------------------------GOOGLE MAPS------------------------------*/
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (LocationListener) this);
        }
        catch(SecurityException e)
        {
            e.printStackTrace();
        }

        // Add a marker in Sydney and move the camera


    }



    @Override
    public void onLocationChanged(Location location)
    {
//        locationText.setText("Latitude: " + location.getLatitude() + "\n Longitude: " + location.getLongitude());
        LatLng LatLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(LatLng)
                .title("Marker in Sydney"));

        mMap.setMaxZoomPreference(20.0f);
        mMap.setMinZoomPreference(6.0f);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng,12.0f));
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            s=addresses.get(0).getAddressLine(0).toString();


//          String t=addresses.get(0).getAddressLine(1).toString();
//           String q= addresses.get(0).getAddressLine(2).toString();

//            locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0).toString());
        }catch(Exception e)
        {

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

//        Toast.makeText(HomeActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {

    }



    //-------------------------CAMERA-----------------------------

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQ_CODE_CAMERA_PERMISSION) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCamera(mCameraConfig);
            } else {
                Toast.makeText(this, "error_camera_permission_denied", Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void take()
    {

    }


    public void onImageCapture(@NonNull final File imageFile) {

        // Convert file to bitmap.
        // Do something.
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);

        //Display the image to the image view
        ((ImageView) findViewById(R.id.img1)).setImageBitmap(bitmap);
        t=imageFile.getAbsolutePath();
//        ((ImageView)findViewById(R.id.img1)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String s=imageFile.getAbsolutePath();
//                Intent intent=new Intent(Main3Activity.this,MainActivity.class);
//                intent.putExtra("key",s);
//                startActivity(intent);
//            }
//        });
    }

    @Override
    public void onCameraError(@CameraError.CameraErrorCodes int errorCode) {
        switch (errorCode) {
            case CameraError.ERROR_CAMERA_OPEN_FAILED:
                //Camera open failed. Probably because another application
                //is using the camera
                Toast.makeText(this, "error_cannot_open", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_IMAGE_WRITE_FAILED:
                //Image write failed. Please check if you have provided WRITE_EXTERNAL_STORAGE permission
                Toast.makeText(this, "error_cannot_write", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_CAMERA_PERMISSION_NOT_AVAILABLE:
                //camera permission is not available
                //Ask for the camera permission before initializing it.
                Toast.makeText(this, "error_cannot_get_permission", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_DOES_NOT_HAVE_OVERDRAW_PERMISSION:
                //Display information dialog to the user with steps to grant "Draw over other app"
                //permission for the app.
                HiddenCameraUtils.openDrawOverPermissionSetting(this);
                break;
            case CameraError.ERROR_DOES_NOT_HAVE_FRONT_CAMERA:
                Toast.makeText(this, "error_not_having_camera", Toast.LENGTH_LONG).show();
                break;
        }
    }



}
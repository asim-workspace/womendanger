package com.example.lenovo.womendangerrecovery.Profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.womendangerrecovery.R;
import com.example.lenovo.womendangerrecovery.Utils.BottomNavigationViewHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class ProfileActivity extends AppCompatActivity {
    private static final String TAG="ProfileActivity";
    private static final int ACTIVITY_NUM=2;
    private Context mContext=ProfileActivity.this;
    private String Pusername,Pmobile,Pemail,userN,mobileN,emailI;
//    private TextView e1,e2,e3;
    private EditText e1,e2, e4,e5;
    private Button b1;
    private ImageView i1,i2,i3;
    private TextView t1,t2,t3,e3;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
//    private users_profile_info musers_profile_info;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
//        e1=findViewById(R.id.username);
//        e2=findViewById(R.id.mobile_number);
//        e3=findViewById(R.id.email_id);
//        e4=findViewById(R.id.username);
//        e5=findViewById(R.id.mobile_number);
////        e6=findViewById(R.id.email_id);
//        i1=findViewById(R.id.updateusername);
////        t1=findViewById(R.id.uname);
////        t2=findViewById(R.id.monum);
////        t3=findViewById(R.id.emaili);
//        i1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (e4.equals(""))
//                {
//                    Toast.makeText(mContext, "Please add your Name First..", Toast.LENGTH_SHORT).show();
//                }else {
//
//
//                    updatUsername();
//                }
//            }
//        });
//        i2=findViewById(R.id.updatemobile);
//        i2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (e5.equals(""))
//                {
//                    Toast.makeText(mContext, "Please add your number First..", Toast.LENGTH_SHORT).show();
//                }else {
//
//
//                    updatePhone();
//                }
//            }
//        });
////        i3=findViewById(R.id.updateemail);
////        i3.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if (e6.equals(""))
////                {
////                    Toast.makeText(mContext, "Please add your Email id First..", Toast.LENGTH_SHORT).show();
////                }else {
////
////
////                    updateEmailid();
////                }
////            }
////        });
//
//
    setupBottomNavigationView();
//    setupFirebaseAuth();
    }

    public void setupBottomNavigationView()
    {
        Log.d(TAG, "setupBottomNavigationView: Settinh up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx= findViewById(R.id.navigation);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext,bottomNavigationViewEx);
        Menu menu=bottomNavigationViewEx.getMenu();
        MenuItem menuItem=menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }


//    public void updatUsername()
//    {
//        userN=e4.getText().toString();
//        myRef.child("user_account_settings").child(mAuth.getCurrentUser().getUid()).child("username").setValue(userN);
//        e4.setText(userN);
//        Toast.makeText(mContext, "Updated Successfully..", Toast.LENGTH_SHORT).show();
//
//
//    }
//    public void updatePhone()
//    {
//        mobileN=e5.getText().toString();
//        myRef.child("user_account_settings").child(mAuth.getCurrentUser().getUid()).child("phone_number").setValue(mobileN);
//        e5.setText(mobileN);
//        Toast.makeText(mContext, "Updated Successfully..", Toast.LENGTH_SHORT).show();
//
//
//    }
////    public void updateEmailid()
////    {
////        emailI=e6.getText().toString();
////        myRef.child("user_account_settings").child(mAuth.getCurrentUser().getUid()).child("email").setValue(emailI);
////        e6.setText(emailI);
////        Toast.makeText(mContext, "Updated Successfully..", Toast.LENGTH_SHORT).show();
////
////
////    }
//
//
//    /*------------------------------------FIREBASE DATABASE-------------------------------*/
//    private void setupFirebaseAuth() {
//        Log.d(TAG, "setupFirebaseAuth: Setting up Firebase Auth");
//
//        mAuth = FirebaseAuth.getInstance();
//        mFirebaseDatabase = FirebaseDatabase.getInstance();
//        myRef = mFirebaseDatabase.getReference();
//        mAuthListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//
//
//                if (user != null) {
//                    Log.d(TAG, "onAuthStateChanged: signed in:" + user.getUid());
//                } else {
//                    Log.d(TAG, "onAuthStateChanged: sign out:");
//                }
//
//            }
//        };
//
//        myRef= FirebaseDatabase.getInstance().getReference();
//        myRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                for (DataSnapshot ds : dataSnapshot.getChildren()) {
////                    d= String.valueOf(myRef.child("users_profile_info"));
////                    if(d.isEmpty()) {
////                    String Uid =mAuth.getCurrentUser().getUid();
//                    user_account_settings upi = ds.child(mAuth.getCurrentUser().getUid()).getValue(user_account_settings.class);
////                    String d= String.valueOf(myRef.child(mAuth.getCurrentUser().getUid()).child("hemail1"));
////                    if (myRef.getDatabase().equals(null))
////                    {
//                        Pusername= upi.getUsername();
//                        Pmobile= upi.getPhone_number();
//                        Pemail= upi.getEmail();
//
//                        e1.setText(Pusername);
//                        e2.setText(Pmobile);
//                        e3.setText(Pemail);
//
////                    }else
////                    {
////                        Toast.makeText(mContext, "update data first!!", Toast.LENGTH_SHORT).show();
////                    }
//
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//
//
//
//    }
//
//    public void onStart() {
//        super.onStart();
//        mAuth.addAuthStateListener(mAuthListener);
//
//    }
//
//    public void onStop() {
//        super.onStop();
//        if (mAuthListener != null)
//            mAuth.removeAuthStateListener(mAuthListener);
//
//    }
}

//    public void usersprofileinfo()
//    {
//        Hemail1=e4.getText().toString();
//        Hmobile1=e5.getText().toString();
//        Hemail2=e6.getText().toString();
//        Hmobile2=e7.getText().toString();
//        Hemail3=e8.getText().toString();
//        Hmobile3=e9.getText().toString();
//
////        if(Hemail1.equals("")||Hmobile1.equals("")||Hemail2.equals("")||Hmobile2.equals("")||Hemail3.equals("")||Hmobile3.equals(""))
////        {
////            Toast.makeText(mContext, "Required(*) fields must be fill..!!", Toast.LENGTH_SHORT).show();
////        }else  {
//        if (!musers_profile_info.getHemail1().equals(Hemail1))
//        {
//            updateUserProfileInfo(Hemail1,"","","","","");
//        }
//        if (!musers_profile_info.getHmobile1().equals(Hmobile1))
//        {
//            updateUserProfileInfo("",Hmobile1,"","","","");
//        }
//        if (!musers_profile_info.getHemail2().equals(Hemail2))
//        {
//            updateUserProfileInfo("","",Hemail2,"","","");
//        }
//        if (!musers_profile_info.getHmobile2().equals(Hmobile2))
//        {
//            updateUserProfileInfo("","","",Hmobile2,"","");
//        }
//        if (!musers_profile_info.getHemail3().equals(Hemail3))
//        {
//            updateUserProfileInfo("","","","",Hemail3,"");
//        }
//        if (!musers_profile_info.getHmobile3().equals(Hmobile3))
//        {
//            updateUserProfileInfo("","","","","",Hmobile3);
//        }
//        {
//            users_profile_info upi=new users_profile_info(Hemail1,Hmobile1,Hemail2,Hmobile2,Hemail3,Hmobile3);
//            myRef.child(mContext.getString(R.string.dbname_users_profile_info)).child(mAuth.getCurrentUser().getUid()).setValue(upi);
//
//
//            Intent intent=new Intent(ProfileActivity.this, HomeActivity.class);
//            startActivity(intent);
//            Toast.makeText(mContext, "informations Successfully Saved!!!...Thank You", Toast.LENGTH_SHORT).show();
//
//
//        }



//    }






//    public void updateUserProfileInfo(String Hemail1,String Hmobile1,String Hemail2,String Hmobile2,String Hemail3,String Hmobile3)
//    {
//        Log.d(TAG, "updateUserAccountSettings: updating user account settings");
//
//        if (Hemail1!=null)
//        {
//            myRef.child(mContext.getString(R.string.dbname_users_profile_info))
//                    .child(mAuth.getCurrentUser().getUid())
//                    .child(mContext.getString(R.string.field_hemail1))
//                    .setValue(Hemail1);
//
//        }
//        if (Hmobile1!=null)
//        {
//            myRef.child(mContext.getString(R.string.dbname_users_profile_info))
//                    .child(mAuth.getCurrentUser().getUid())
//                    .child(mContext.getString(R.string.field_hmobile1))
//                    .setValue(Hmobile1);
//        }
//        if (Hemail2!=null)
//        {
//            myRef.child(mContext.getString(R.string.dbname_users_profile_info))
//                    .child(mAuth.getCurrentUser().getUid())
//                    .child(mContext.getString(R.string.field_hemail2))
//                    .setValue(Hemail2);
//        }
//
//        if (Hmobile2!=null) {
//            myRef.child(mContext.getString(R.string.dbname_users_profile_info))
//                    .child(mAuth.getCurrentUser().getUid())
//                    .child(mContext.getString(R.string.field_hmobile2))
//                    .setValue(Hmobile2);
//        }
//        if (Hemail3!=null)
//        {
//            myRef.child(mContext.getString(R.string.dbname_users_profile_info))
//                    .child(mAuth.getCurrentUser().getUid())
//                    .child(mContext.getString(R.string.field_hemail3))
//                    .setValue(Hemail3);
//        }
//
//        if (Hmobile3!=null) {
//            myRef.child(mContext.getString(R.string.dbname_users_profile_info))
//                    .child(mAuth.getCurrentUser().getUid())
//                    .child(mContext.getString(R.string.field_hmobile3))
//                    .setValue(Hmobile3);
//        }
//
//    }



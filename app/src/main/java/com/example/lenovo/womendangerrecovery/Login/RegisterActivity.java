package com.example.lenovo.womendangerrecovery.Login;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.womendangerrecovery.Models.user_account_settings;
import com.example.lenovo.womendangerrecovery.Models.users_profile_info;
import com.example.lenovo.womendangerrecovery.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;



public class RegisterActivity extends AppCompatActivity {
    public static final String TAG="RegisterActivity";
    private EditText e1,e2,e3,e4;
    private String username,email,pass,phone,email1,mobile1,email2,mobile2,email3,mobile3;
    private String append="";
    private Button b1;
    private TextView t1;
    private String UserId;
    private Context mContext;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        FirebaseApp.initializeApp(this);
        mContext=RegisterActivity.this;
        email1="add email";
        mobile1="add mobile no.";
        email2="add email";
        mobile2="add mobile no.";
        email3="add email";
        mobile3="add mobile no.";
        e1=findViewById(R.id.input_name);
        e2=findViewById(R.id.input_email);
        e3=findViewById(R.id.input_password);
        e4=findViewById(R.id.input_phone);
        b1=findViewById(R.id.btn_signup);
        t1=findViewById(R.id.link_login);
        dialog=new ProgressDialog(this);
        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext,LoginActivity.class);
                startActivity(intent);
            }
        });
        //        mAuth=FirebaseAuth.getInstance();
        init();
        setupFirebaseAuth();


    }

    public void registerNewEmail(final String email, String pass, final String username, final String phone) {
        mAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete( Task<AuthResult> task) {

                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Toast.makeText(mContext, "auth_failed", Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign in fails, display a message to the user.
                            //Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            UserId = mAuth.getCurrentUser().getUid();
                            Toast.makeText(RegisterActivity.this, "Authentication Successful!!Sending Verification Email.....", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            Log.d(TAG, "onComplete: AuthState changed" + UserId);
                            sendVerificationEmail();
                            users(username,email,phone);
                            users2(email1,mobile1,email2,mobile2,email3,mobile3);
                        }
//
                    }
                });
    }

    public void sendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(Task<Void> task) {
                            if (task.isSuccessful()) {

                            } else {
                                Toast.makeText(mContext, "couldn't send verfivation email", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }



    public void users(String username, String email, String phone_number)
    {
        UserId=mAuth.getCurrentUser().getUid();
        user_account_settings settings=new user_account_settings(username,email,phone_number);
        myRef.child("user_account_settings").child(UserId).setValue(settings);

    }
    public void users2(String email1,String mobile1,String email2,String mobile2,String email3,String mobile3)
    {
        users_profile_info upi=new users_profile_info(email1,mobile1,email2,mobile2,email3,mobile3);
        myRef.child("users_profile_info").child(UserId).setValue(upi);
    }


    private boolean checkInputs(String username,String email,String password,String phone)
    {
        if(email.equals("")||username.equals("")||password.equals("")||phone.equals(""))
        {
            Toast.makeText(mContext,"You must fill all fields",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void init()
    {
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username=e1.getText().toString();
                email=e2.getText().toString();
                pass=e3.getText().toString();
                phone=e4.getText().toString();

//                Intent intent=new Intent(mContext, MainActivity.class);
//                startActivity(intent);
                if(checkInputs(email,pass,username,phone))
                {
                    dialog.setMessage("Please Wait...");
                    dialog.show();
                    registerNewEmail(email,pass,username,phone);
                }

            }
        });
    }

    /*

   -------------------------------firebase---------------------------------
    */
    private void checkIfUsernameExists(final String username) {
        Log.d(TAG, "checkIfUsernameExists: Chicking if "+username+"already exists");
        DatabaseReference reference=FirebaseDatabase.getInstance().getReference();
        Query query=reference.child("user_account_settings")
                .orderByChild("username")
                .equalTo(username);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot singleSnapShot : dataSnapshot.getChildren()) {
                    if (singleSnapShot.exists()) {
                        Log.d(TAG, "checkIfUsernameExists: Founf a match " + singleSnapShot.getValue(user_account_settings.class).getUsername());
                        append = myRef.push().getKey().substring(3, 10);
                        Log.d(TAG, "onDataChange: username already exist appending random string to name:" + append);
                    }

                }
                String e1 = "";
                e1 = username + append;
                //users(e1,email, "","","", String.valueOf(2));
                Toast.makeText(mContext, "Signup successful sending verification email", Toast.LENGTH_SHORT).show();
                mAuth.signOut();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void setupFirebaseAuth()
    {
        Log.d(TAG, "setupFirebaseAuth: Setting up Firebase Auth");
        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase=FirebaseDatabase.getInstance();
        myRef=mFirebaseDatabase.getReference();
        mAuthListener=new FirebaseAuth.AuthStateListener()
        {
            @Override
            public void onAuthStateChanged( FirebaseAuth firebaseAuth) {
                final FirebaseUser user=firebaseAuth.getCurrentUser();


                if(user!=null) {
                    Log.d(TAG, "onAuthStateChanged: signed in:" + user.getUid());
                    myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            checkIfUsernameExists(username);
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    finish();
                }
                else
                {
                    Log.d(TAG, "onAuthStateChanged: sign out:");
                }

            }
        };
    }
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    public void onStop()
    {
        super.onStop();
        if(mAuthListener!=null)
            mAuth.removeAuthStateListener(mAuthListener);
    }
}
package com.example.lenovo.womendangerrecovery.Settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lenovo.womendangerrecovery.Home.HomeActivity;
import com.example.lenovo.womendangerrecovery.Models.users_profile_info;
import com.example.lenovo.womendangerrecovery.R;
import com.example.lenovo.womendangerrecovery.Utils.BottomNavigationViewHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class addFragmentActivity extends Fragment
{
    private static final String TAG="addFragmentActivity";
    private String Hemail1,Hmobile1,Hemail2,Hmobile2,Hemail3,Hmobile3;
    private EditText e1,e2,e3,e4,e5,e6;
    private Button b1;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private View view;

    public addFragmentActivity() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_addfragment, container, false);

        e1=view.findViewById(R.id.help1);
        e2=view.findViewById(R.id.help11);
        e3=view.findViewById(R.id.help2);
        e4=view.findViewById(R.id.help22);
        e5=view.findViewById(R.id.help3);
        e6=view.findViewById(R.id.help44);
        b1=view.findViewById(R.id.addbutton);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (e1.getText().toString().equals("")||e2.getText().toString().equals("")||e3.getText().toString().equals("")||e4.getText().toString().equals("")||e5.getText().toString().equals("")||e6.getText().toString().equals(""))
                {
                    Toast.makeText(getActivity(), "you must be fill all the fiels", Toast.LENGTH_SHORT).show();
                }else{
                    addPeople();
                }

            }
        });

        setupFirebaseAuth();



        return view;


    }

    public void addPeople()
    {
        Hemail1=e1.getText().toString();
        Hmobile1=e2.getText().toString();
        Hemail2=e3.getText().toString();
        Hmobile2=e4.getText().toString();
        Hemail3=e5.getText().toString();
        Hmobile3=e6.getText().toString();

        if(Hemail1.equals("")&&Hmobile1.equals("")&&Hemail2.equals("")&&Hmobile2.equals("")&&Hemail3.equals("")&&Hmobile3.equals(""))
        {
            Toast.makeText(getActivity(), "Required(*) fields must be fill..atleast one detail needed!!", Toast.LENGTH_SHORT).show();
        }else  {
            users_profile_info upi=new users_profile_info(Hemail1,Hmobile1,Hemail2,Hmobile2,Hemail3,Hmobile3);
            myRef.child("users_profile_info").child(mAuth.getCurrentUser().getUid()).setValue(upi);
            Toast.makeText(getActivity(), "informations Successfully Saved!!!...Thank You", Toast.LENGTH_SHORT).show();

        }
    }




    /*------------------------------------FIREBASE DATABASE-------------------------------*/


    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: Setting up Firebase Auth");

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();


                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged: signed in:" + user.getUid());
                } else {
                    Log.d(TAG, "onAuthStateChanged: sign out:");
                }

            }
        };


    }

    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

    }

    public void onStop() {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);

    }

}

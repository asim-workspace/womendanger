package com.example.lenovo.womendangerrecovery;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.lenovo.womendangerrecovery.Login.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {

            public void run() {



               /* if (mAuth.getCurrentUser().isEmailVerified())
                {
                    Intent intent=new Intent(Splash.this,HomeActivity.class);
                    startActivity(intent);



                } else {*/
                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();

//                startActivity(new Intent(SplashScreen.this, SecondActivity.class));
//                finish();
            }
        }, secondsDelayed * 5000);
    }



}

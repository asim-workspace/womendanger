package com.example.lenovo.womendangerrecovery.Models;


public class user_account_settings
{
    private String username;
    private String email;
    private String phone_number;


    public user_account_settings(String username, String email, String phone_number) {
        this.username = username;
        this.email = email;
        this.phone_number = phone_number;

    }

    public user_account_settings()
    {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }


    @Override
    public String toString() {
        return "user_account_settings{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", phone_number=" + phone_number +
                '}';
    }
}

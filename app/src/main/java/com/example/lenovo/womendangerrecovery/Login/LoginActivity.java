package com.example.lenovo.womendangerrecovery.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.example.lenovo.womendangerrecovery.Profile.ProfileActivity;
import com.example.lenovo.womendangerrecovery.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class LoginActivity extends AppCompatActivity {
    TextView t1,t2;
    private static final String TAG="LoginActivity";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private LoginActivity mContext;
    private ProgressBar mProgressBar;
    private EditText mEmail,mPassword;
    private TextView mPleaseWait;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dialog=new ProgressDialog(this);
        t1=findViewById(R.id.link_signup);

        mAuth = FirebaseAuth.getInstance();
        mEmail= findViewById(R.id.input_email);
        mPassword= findViewById(R.id.input_password);
        mContext=LoginActivity.this;
        Log.d(TAG, "onCreate: Started");
        setupFirebaseAuth();
        init();

        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });


    }
    private boolean isNullString( String string)
    {
        Log.d(TAG, "isNullString: checking if String nill");
        return string.equals("");
    }
    /*

  ----------------------------------firebase----------------------------
   */
    private void init()
    {
        Button btnLogin= findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Attempting to Login");
                
                String email=mEmail.getText().toString();
                String password=mPassword.getText().toString();
                dialog.setMessage("please wait...");
                dialog.show();
                if(isNullString(email)&&isNullString(password))
                {
                    Toast.makeText(mContext,"You must fill out all the fields",Toast.LENGTH_SHORT).show();
                }

                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInWithEmail:success" +task.isSuccessful());
                                FirebaseUser user=mAuth.getCurrentUser();


                                if (!task.isSuccessful())
                                {
                                    Log.d(TAG, "SigninWithEmail: Failed",task.getException());
                                    Toast.makeText(mContext,"auth_failed",Toast.LENGTH_SHORT).show();
//                                    mProgressBar.setVisibility(View.GONE);
//                                    mPleaseWait.setVisibility(View.GONE);
                                } else {
                                    try{
                                        if(user.isEmailVerified()){
                                            Log.d(TAG, "onComplete: Success Email is verfied");
                                            dialog.dismiss();
                                            Intent intent=new Intent(mContext,ProfileActivity.class);
                                            startActivity(intent);
                                        }else{
                                            Toast.makeText(mContext, "Email is not verified\n check your email first", Toast.LENGTH_SHORT).show();
//                                            mProgressBar.setVisibility(View.GONE);
//                                            mPleaseWait.setVisibility(View.GONE);
                                        }
                                    }catch (NullPointerException e){
                                        Log.e(TAG, "onComplete: NullPointerException"+e.getMessage() );
                                        mAuth.signOut();
                                    }

                                }

                                // ...
                            }
                        });

            }
        });

        if(mAuth.getCurrentUser()!=null)
        {
            Intent intent=new Intent(LoginActivity.this, ProfileActivity.class);
            startActivity(intent);
            finish();

        }
    }
    private void setupFirebaseAuth()
    {
        Log.d(TAG, "setupFirebaseAuth: Setting up Firebase Auth");
        mAuth = FirebaseAuth.getInstance();
        mAuthListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
                FirebaseUser user=firebaseAuth.getCurrentUser();


                if(user!=null) {
                    Log.d(TAG, "onAuthStateChanged: signed in:" + user.getUid());
                }
                else
                {
                    Log.d(TAG, "onAuthStateChanged: sign out:");
                }

            }
        };
    }
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    public void onStop()
    {
        super.onStop();
        if(mAuthListener!=null)
            mAuth.removeAuthStateListener(mAuthListener);
    }


}

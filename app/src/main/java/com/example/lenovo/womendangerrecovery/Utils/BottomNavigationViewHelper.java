package com.example.lenovo.womendangerrecovery.Utils;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;

import com.example.lenovo.womendangerrecovery.Home.HomeActivity;
import com.example.lenovo.womendangerrecovery.Profile.ProfileActivity;
import com.example.lenovo.womendangerrecovery.R;
import com.example.lenovo.womendangerrecovery.Settings.OPtionsActivity;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;



public class BottomNavigationViewHelper {
    private static final String TAG="BottomNavigationViewHel";
    public static void setupBottomNavigationView(BottomNavigationViewEx bottomNavigationViewEx)
    {
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setTextVisibility(false);
    }
    public static void enableNavigation(final Context context,BottomNavigationViewEx view)
    {
        view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.bottomone:
                        Intent intent1=new Intent(context,HomeActivity.class);

                        context.startActivity(intent1);
//                    mTextMessage.sotepadnoetText(R.string.title_home);
                        break;
                    case R.id.bottomtwo:
                        Intent intent2=new Intent(context, OPtionsActivity.class);
                        context.startActivity(intent2);
//                    mTextMessage.setText(R.string.title_dashboard);
                        break;

                    case R.id.bottomthree:
                        Intent intent3=new Intent(context, ProfileActivity.class);
                        context.startActivity(intent3);
//                    mTextMessage.setText(R.string.title_notifications);

                        break;

                }
                return false;
            }
        });
    }
}

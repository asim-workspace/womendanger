package com.example.lenovo.womendangerrecovery.Settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.womendangerrecovery.Models.users_profile_info;
import com.example.lenovo.womendangerrecovery.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class savedPeopleActivity extends Fragment
{
    private static final String TAG="savedPeopleActivity";
    private Context mContext=getActivity();
    private String Hemail1,Hmobile1,Hemail2,Hmobile2,Hemail3,Hmobile3;
    private TextView e1,e2,e3,e4,e5,e6;
    private Button b1;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference myRef;
    private static String d;

    public savedPeopleActivity() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_savedfragment, container, false);

        e1=view.findViewById(R.id.help1);
        e2=view.findViewById(R.id.help11);
        e3=view.findViewById(R.id.help2);
        e4=view.findViewById(R.id.help22);
        e5=view.findViewById(R.id.help3);
        e6=view.findViewById(R.id.help44);

        setupFirebaseAuth();

        return view;
    }





    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: Setting up Firebase Auth");

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();


                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged: signed in:" + user.getUid());
                } else {
                    Log.d(TAG, "onAuthStateChanged: sign out:");
                }

            }
        };
//          myRef= FirebaseDatabase.getInstance().getReference("user_account_settings");

        myRef= FirebaseDatabase.getInstance().getReference();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    users_profile_info upi = ds.child(mAuth.getCurrentUser().getUid()).getValue(users_profile_info.class);

                    Hemail1 = upi.getHemail1();
                    Hmobile1 = upi.getHmobile1();
                    Hemail2 = upi.getHemail2();
                    Hmobile2 = upi.getHmobile2();
                    Hemail3 = upi.getHemail3();
                    Hmobile3 = upi.getHmobile3();

//                    dialog.dismiss();
//                    if(Hemail1.isEmpty()&&Hmobile1.isEmpty()&&Hemail2.isEmpty()&&Hmobile2.isEmpty()&&Hemail3.isEmpty()&&Hmobile3.isEmpty())
//                    {
//                        Toast.makeText(mContext, "please add emergency contacts", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
                    e1.setText(Hemail1);
                    e2.setText(Hmobile1);
                    e3.setText(Hemail2);
                    e4.setText(Hmobile2);
                    e5.setText(Hemail3);
                    e6.setText(Hmobile3);
//                        }else
//                        {
//                            Toast.makeText(getActivity(), "Update your data first", Toast.LENGTH_SHORT).show();
//                        }
//                    e4.setText(address);
//                    circleImageView.setImageURI(Uri.parse(photo));
//                    String ssname=username;
//                   Intent intent=new Intent(getActivity(),Account_Activity.class);
//                   intent.putExtra("uname",ssname);
//                   startActivity(intent);
//                   }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

    }

    public void onStop() {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);

    }

}


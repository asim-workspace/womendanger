package com.example.lenovo.womendangerrecovery.Settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.lenovo.womendangerrecovery.Login.LoginActivity;
import com.example.lenovo.womendangerrecovery.R;
import com.example.lenovo.womendangerrecovery.Utils.BottomNavigationViewHelper;
import com.example.lenovo.womendangerrecovery.Utils.SectionPageradapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class OPtionsActivity extends AppCompatActivity {
    private static final String TAG="OPtionActivity";
    private static final int ACTIVITY_NUM=1;
    private Context mContext=OPtionsActivity.this;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);


        setupFirebaseAuth();
        setupBottomNavigationView();
        setupViewPager();
    }

    private void setupViewPager()
    {
        SectionPageradapter adapter=new SectionPageradapter(getSupportFragmentManager());
        adapter.addFragment(new addFragmentActivity());
        adapter.addFragment(new savedPeopleActivity());
        ViewPager viewPager=findViewById(R.id.viewpager_container);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout=findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_add);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_saved);
    }


    public void setupBottomNavigationView()
    {
        Log.d(TAG, "setupBottomNavigationView: Settinh up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx= findViewById(R.id.navigation);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext,bottomNavigationViewEx);
        Menu menu=bottomNavigationViewEx.getMenu();
        MenuItem menuItem=menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }

    /*

   ----------------------------------firebase----------------------------
    */private void checkCurrentUser(FirebaseUser user)
    {
        Log.d(TAG, "checkCurrentUser: checking if user is logged in: ");
        if(user==null)
        {
            Intent intent=new Intent(mContext, LoginActivity.class);
            startActivity(intent);
        }
    }
    private void setupFirebaseAuth()
    {
        Log.d(TAG, "setupFirebaseAuth: Setting up Firebase Auth");
        mAuth = FirebaseAuth.getInstance();
        mAuthListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
                FirebaseUser user=firebaseAuth.getCurrentUser();

                checkCurrentUser(user);

                if(user!=null) {
                    Log.d(TAG, "onAuthStateChanged: signed in:" + user.getUid());
                }
                else
                {
                    Log.d(TAG, "onAuthStateChanged: signed_out:");
                }

            }
        };
    }
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        checkCurrentUser(mAuth.getCurrentUser());
    }
    public void onStop()
    {
        super.onStop();
        if(mAuthListener!=null)
            mAuth.removeAuthStateListener(mAuthListener);
    }

}

